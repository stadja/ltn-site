<?php
$listOfPages = [
	['label' => "accueil", 'icon' => 'home', 'title' => 'Accueil', 'external' => false],
	['label' => "services", 'icon' => 'men9', 'title' => 'Nos services', 'external' => false],
	['label' => "realisations", 'icon' => 'web-programming', 'title' => 'Nos réalisations', 'external' => false],
	['label' => "presentation", 'icon' => 'accessory192', 'title' => 'Pr&eacute;sentation', 'external' => false],
	['label' => "contact", 'icon' => 'businessmen6', 'title' => 'Contact', 'external' => false],
	['label' => "journal", 'icon' => 'pen-feather-black-diagonal-shape-of-a-bird-wing', 'title' => 'Journal', 'external' => false],
];

if (sizeof($_POST) > 0) {
	if (isset($_POST['emailInput']) && $_POST['emailInput']) {
		die('Hack ?');
	}

	$str = 'Email de la part de: <ul>';
	$str .= '<li>' . $_POST['nameInput'] . '</li>';
	$str .= '<li>' . $_POST['contactInput'] . '</li>';
	$str .= '</ul><br/>';
	$str .= nl2br($_POST['wordInput']);

	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: LTN.agency <no-reply@ltn.agency>' . "\r\n";
	mail('contact@ltn.agency', 'un contact depuis le site LTN', $str, $headers);

	header('Location: ?courriel=1#contact');
}

$fullUrl   = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$url       = $uri_parts[0];

$anchor = false;
if (isset($_GET['page'])) {
	$anchor = $_GET['page'];
}
$url = str_replace('/' . $anchor, '/', $url);

$realisation = false;
if (isset($_GET['realisation'])) {
	$realisation = $_GET['realisation'];
	$url         = str_replace('/' . $realisation, '', $url);
}

$article = false;
if (isset($_GET['article'])) {
	$article = $_GET['article'];
	$url     = str_replace('/' . $article, '', $url);
}

$domain = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $url;

?><!DOCTYPE html>
<html lang="fr">
<head itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# business: http://ogp.me/ns/business#">
    <meta charset="utf-8">
    <meta name="google-site-verification" content="HF0MPdWHI4N3iHga0uFQoLXF4STJlgw0c-o9FNrwOf0" />
    <meta name="google-site-verification" content="qXFtWOw7M_z-ejJEJBjkRW8_SYNRzy3sMroZadFguLQ" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:image" content="<?php echo $fullUrl; ?>tarsier.php?color=090909&background=6B91AB" />
    <meta property="og:type" content="business.business" />
    <meta property="fb:app_id" content="1832606063638916" />
    <meta property="business:contact_data:street_address" content="32 rue des vaures" />
    <meta property="business:contact_data:locality" content="Bergerac" />
    <meta property="business:contact_data:postal_code" content="24100" />
    <meta property="business:contact_data:country_name" content="France" />
    <meta property="fb:pages" content="letarsiernumerique" />
    <meta property="og:url" content="<?php echo $fullUrl; ?>" />
    <meta property="og:title" content="LTN - Le Tarsier Num&eacute;rique" />
    <meta property="og:description" content="Stratégie et Développement Web. Création de sites Internet, aide pour vos projets :
    LTN, votre entreprise à Bergerac en Dordogne avec une présence internationale et qui accompagne vos ambitions numériques." />

    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

    <title itemprop='name'>LTN - Création de site Internet, Stratégie et Développement Web en Dordogne</title>
    <link rel="stylesheet" href="<?php echo $url; ?>css/font/flaticon.css">
    <!-- <link rel="stylesheet" type="text/css" href="css/grid.css"> -->
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <link rel="shortcut icon" href="favicon.png" />
    <link href='//fonts.googleapis.com/css?family=Lato:400i,700|Quicksand:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo $url; ?>css/normalize.min.css">
    <link rel="stylesheet" href="c<?php echo $url; ?>ss/main.css">
    <script src="<?php echo $url; ?>js/modernizr-custom.js"></script>

    <style type="text/css" media="screen">
        /* latin */
        <?php echo file_get_contents('css/grid.css'); ?>


        content {
            width: <?php echo sizeof($listOfPages); ?>00vw;
        }

        <?php foreach ($listOfPages as $key => $pageArray): ?>
            #<?php echo $pageArray['label']; ?>:target content {
                margin-left: calc(<?php echo $key; ?> * -95vw + <?php echo $key ? (sizeof($listOfPages) - 1 == $key ? 5 : 2.5) : 0; ?>vw);
                background-color: #<?php echo $key % 2 ? 'bcd5e2' : '6b91ab'; ?>;
            }
        <?php endforeach;?>

        <?php echo file_get_contents('css/style.min.css'); ?>
    </style>
</head>
<body>

    <nav id="smallNav" class="fixed">
        <i id="burgerOpen" class="burger fixed icon fa flaticon-menu" aria-hidden="true"></i>
        <i id="burgerClose" class="burger fixed icon fa flaticon-cancel" aria-hidden="true"></i>
        <ul class="fixed">
            <?php foreach ($listOfPages as $pageArray): ?>
                <li>
                    <?php if ($pageArray['external']): ?>
                        <a id="<?php echo $pageArray['label']; ?>Link" href="/<?php echo $pageArray['label']; ?>">
                    <?php else: ?>
                        <a id="<?php echo $pageArray['label']; ?>Link" href="<?php echo $url; ?>#section-<?php echo $pageArray['label']; ?>">
                    <?php endif;?>

                        <icone>
                            <i class="icon fa flaticon-<?php echo $pageArray['icon']; ?>" aria-hidden="true"></i>
                        </icone><br/>
                        <label><?php echo $pageArray['title']; ?></label>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </nav>
    <?php foreach ($listOfPages as $pageArray): ?>
    <div id="<?php echo $pageArray['label']; ?>">
    <?php endforeach;?>
        <content-wrapper itemscope itemtype="http://schema.org/Corporation">
            <span itemscope itemprop="ContactPoint" itemtype="http://schema.org/ContactPoint">
                <meta itemprop="telephone" content= "+33.(0)6.38.84.33.19">
                <meta itemprop="email" content= "contact@ltn.agency">
                <meta itemprop="contactType" content= "customer service">
                <meta itemprop="availableLanguage" content= "fr, en, es">
            </span>

            <nav id="mainNav">
                <ul>
                <?php foreach ($listOfPages as $pageArray): ?>
                    <li>
                    <?php if ($pageArray['external']): ?>
                        <a itemprop="url" content="<?php echo $fullUrl; ?>" id="<?php echo $pageArray['label']; ?>Link" href="<?php echo $url; ?><?php echo $pageArray['label']; ?>">
                    <?php else: ?>
                        <a itemprop="url" content="<?php echo $fullUrl; ?>" id="<?php echo $pageArray['label']; ?>Link" href="<?php echo $url; ?>#<?php echo $pageArray['label']; ?>">
                    <?php endif;?>
                            <icone>
                                <i class="icon fa flaticon-<?php echo $pageArray['icon']; ?>" aria-hidden="true"></i>
                            </icone><br/>
                            <label><?php echo $pageArray['title']; ?></label>
                        </a>
                    </li>
                <?php endforeach;?>
                </ul>
            </nav>

            <content>
                <?php foreach ($listOfPages as $pageArray): ?>
                    <?php include 'partials/section-' . $pageArray['label'] . '.php';?>
                <?php endforeach;?>
            </content>

            <footer class="noSwipe" id="bigFooter">
                <span itemprop="legalName">LTN</span> - SASU au capital de 1000&euro;
                | N° TVA : FR 96 81 43 03 582
                | N° SIRET : 814 303 582
                | RIB : 20041 01001 2088761L022 88
            </footer>
        </content-wrapper>
    <?php foreach ($listOfPages as $pageArray): ?>
    </div>
    <?php endforeach;?>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="<?php echo $url; ?>js/grid.js"></script>
    <script>

        var pages = [<?php foreach ($listOfPages as $pageArray): ?>"#<?php echo $pageArray['label']; ?>",<?php endforeach;?>];

        function getCurrentPageNumber() {
            var currentAnchor = document.location.hash;
            var currentPageNumber = pages.indexOf(currentAnchor);
            return currentPageNumber;
        }

        function goRight() {
            var currentPageNumber = getCurrentPageNumber();
            if (currentPageNumber >= (pages.length - 1)) {
                return false;
            }
            goTo(pages[currentPageNumber + 1]);
        }

        function goLeft() {
            var currentPageNumber = getCurrentPageNumber();
            if (currentPageNumber < 1) {
                return false;
            }
            goTo(pages[currentPageNumber - 1]);
        }

        function goTo(anchor) {
            document.location.hash = anchor;
        }

        $(document).keydown(function(event) {
            switch (event.keyCode) {
                case 39:
                goRight();
                break;
                case 37:
                goLeft();
                break;
            }
        });

        // $(function() {
        //     function isMobile() {
        //         try{
        //             document.createEvent("TouchEvent"); return true;
        //         }
        //         catch(e){ return false; }
        //     }

        //     if ( isMobile() == true ) {
        //         $("section-content").swipe( {
        //     //Single swipe handler for left swipes
        //     swipeLeft:function(event, direction, distance, duration, fingerCount) {
        //         goRight();
        //     },
        //     swipeRight:function(event, direction, distance, duration, fingerCount) {
        //         goLeft();
        //     },
        //     //Default is 75px, set to 0 for demo so any distance triggers swipe
        //     threshold:0
        // });
        //     };
        // });


        function updateQueryStringParameter(key, value) {
            var uri = '<?php echo $url; ?>';
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";

            var final = '';
            if (uri.match(re)) {
                final = uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                final = uri;
                if (value) {
                    final = uri + separator + key + "=" + value;
                }
            }

            final = final+window.location.hash
            return final;
        }

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&amp;");
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }

        Grid.init();

        setTimeout(function() {

            <?php if ($realisation): ?>
                var currentRealisation = '<?php echo $realisation ?>';
            <?php else: ?>
                var currentRealisation = <?php $realisation?>getQueryVariable('realisation');
            <?php endif;?>

            if (currentRealisation) {
                console.log(currentRealisation);
                $('#'+currentRealisation).find('.viewer')[0].click();
            }

            <?php if ($article): ?>
                var currentArticle = '<?php echo $article ?>';
            <?php else: ?>
                var currentArticle = <?php $article?>getQueryVariable('article');
            <?php endif;?>

            if (currentArticle) {
                $('.journal-list li a[data-article="'+currentArticle+'"]').click();
            }
        },600);

        var currentPage = getCurrentPageNumber();
        if (currentPage < 0) {
            goTo('<?php echo $anchor ?: 'accueil'; ?>');
        }

        $('#burgerOpen').click(function() {
            $('#smallNav').addClass('opened');
        });

        $('#burgerClose').click(function() {
            $('#smallNav').removeClass('opened');
        });

        $('#smallNav a').click(function() {
            $('#smallNav').removeClass('opened');
        });

        var turnOn = function(elem) {
            // elem.style.display = 'block';
            $(elem).show();

        }

        var turnOff = function(elem) {
            $(elem).hide(200);
            // setTimeout(function() {
            //     this.style.display = 'none';
            // }.bind(elem), 200)
        }

        var clickJournalLink = function(evt) {
            history.pushState('', '', updateQueryStringParameter('article', this.dataset.article));
            $('.toBeRemovedOnSearch').remove();
            $.ajax('<?php echo $url; ?>partials/_journal-posts.php', {
                data: {
                    article: this.dataset.article
                },
                success: function(data) {
                    $('#emptySearch').css('opacity', 1);
                    $('.journal-list-search').empty();
                    $('.journal-list-search').append(data);
                    turnOff($('.journal-list')[0]);
                    turnOn($('.journal-list-search')[0]);
                        $('.journal-list-search tags a').click(initTags);
                        $('.journal-list-search .journal-link').click(clickJournalLink);
                    // turnOn($('.journal-list-search')[0]);
                }
            });

            evt.preventDefault();
            return false;
        };



        var initTags = function(evt) {
            var top = this.offsetTop;
            var left = this.offsetLeft;
            $('.toBeRemovedOnSearch').remove();
            $('.journal-list-search').empty();
            var newNode = this.cloneNode(true);
            newNode.className += " toBeRemovedOnSearch";
            newNode.style.position = 'absolute';
            newNode.style.top = top+'px';
            newNode.style.left = left+'px';
            $('#section-journal section-content').append(newNode);
            $('#emptySearch').css('opacity', 1);
            setTimeout(function() {
                var journal = $('journal')[0];
                this.style.top = (journal.offsetTop + 8)+'px';
                this.style.left = (journal.offsetLeft + 60)+'px';
                turnOff($('.journal-list')[0]);
                $.ajax('<?php echo $url; ?>partials/_journal-posts.php', {
                    data: {
                        tag: this.firstChild.firstChild.innerText
                    },
                    success: function(data) {
                        $('.journal-list-search').append(data);
                        turnOn($('.journal-list-search')[0]);
                        $('.journal-list-search tags a').click(initTags);
                        $('.journal-list-search .journal-link').click(clickJournalLink);
                    }
                });
            }.bind(newNode), 100);
            evt.preventDefault();
            return false;
        };

        $('#emptySearch').click(function(evt) {
            history.pushState('', '', updateQueryStringParameter('article', ''));
            if (!$('.journal-list').children().size()) {
                $.ajax('<?php echo $url; ?>partials/_journal-posts.php', {
                    success: function(data) {
                        $('.journal-list').append(data);
                        $('.journal-list tags a').click(initTags);
                        $('.journal-list .journal-link').click(clickJournalLink);
                    }
                });
            }
            $(this).css('opacity', 0);
            turnOff($('.journal-list-search')[0]);
            setTimeout(function() {
                $('.journal-list-search').empty();
            });
            turnOn($('.journal-list')[0]);
            $('.toBeRemovedOnSearch').remove();
            evt.preventDefault();
            return false;
        });

        $('.journal-list .journal-link').click(clickJournalLink);
        $('.journal-list tags a').click(initTags);

    </script>

    <!-- Piwik -->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(["setDomains", ["*.ltn.agency"]]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//piwik.ltn.io/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '3']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//piwik.ltn.io/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

</body>
</html>
