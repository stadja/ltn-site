<?php
require __DIR__.'/../vendor/autoload.php';
use \Michelf\MarkdownExtra;

$postDir = __DIR__.'/../posts';
$dir     = scandir($postDir, SCANDIR_SORT_DESCENDING);
$posts   = array();
foreach ($dir as $key => $value) {
    if (!in_array($value, array(".", ".."))) {
        // if (is_dir($dir.DIRECTORY_SEPARATOR.$value)) {
        //     $result[$value] = dirToArray($dir.DIRECTORY_SEPARATOR.$value);
        // } else {
        $path  = $postDir.'/'.$value;
        $file  = file($path);
        $title = $file[0];
        $tags  = array();
        foreach (explode(',', $file[1]) as $tag) {
            $tags[] = trim($tag);
        }
        $link = $file[2];

        unset($file[0]);
        unset($file[1]);
        unset($file[2]);
        unset($file[3]);
        $content = implode('', $file);
        $content = MarkdownExtra::defaultTransform($content);

        $posts[] = array(
            'path'    => $postDir.'/'.$value,
            'slug'    => str_replace('.md', '', $value),
            'title'   => $title,
            'tags'    => $tags,
            'content' => $content,
            'link'    => $link
        );

        if (isset($_GET['article']) && $_GET['article']) {
            $articleQuery = strtolower($_GET['article']);
            foreach ($posts as $post) {
                if ($post['slug'] == $articleQuery) {
                    echo $post['content'];
                    return true;
                }
            }
        }

        if (isset($_GET['tag']) && $_GET['tag']) {
            $tagQuery = strtolower($_GET['tag']);
            $posts    = array_filter($posts, function ($post) use ($tagQuery) {
                foreach ($post['tags'] as $tag) {
                    if (strtolower($tag) == $tagQuery) {
                        return true;
                    }
                }
                return false;
            });
        }
    }
}
?>
<?php foreach ($posts as $post): ?>
<li>
    <a href="<?php echo $post['link']; ?>" target="_blank"><h3><?php echo $post['title']; ?></h3></a>
    <tags>
        <?php foreach ($post['tags'] as $tag): ?>
            <a class="tag" href="#journal"><tag><strong><?php echo $tag; ?></strong></tag></a>
        <?php endforeach;?>
    </tags>
    <?php echo $post['content']; ?>
</li>
<?php endforeach;?>
