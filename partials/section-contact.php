<section id="section-contact">
    <a class="changePage" href="#contact"></a>
    <section-content>
        <baseline>
            <strong>Discutons !</strong>
        </baseline>
        <contacts>
            <column>
            <link itemprop="url" href="<?php echo $fullUrl; ?>">
            <a itemprop="sameAs" class="noDeco" href="https://www.facebook.com/letarsiernumerique/" target="_blank"><i class="icon fa flaticon-facebook-logo" aria-hidden="true"></i></a>
            <a itemprop="sameAs" class="noDeco" href="https://plus.google.com/+Jo%C3%ABlIsra%C3%ABlSaintSeurinsurlIsle" target="_blank"><i class="icon fa flaticon-google-plus" aria-hidden="true"></i></a>
            <br/><br/>
            Rentrez directement en contact avec <strong>LTN</strong> !
            <form action="<?php echo $url; ?>" method="post" accept-charset="utf-8" class="noSwipe">
                <label id="nameLabel"><strong>Votre nom ou prénom ?</strong><input type="text" name="nameInput" value="" required></label>
                <label id="contactLabel"><strong>Comment vous contacter ?</strong><input type="text" name="contactInput" value="" placeholder="Votre téléphone ou votre courriel" required></label>
                <label id="emailLabel"><strong>Laissez ce champ vide !</strong><input type="text" name="emailInput" value=""></label>

                <label><strong>En quelques mots, en quoi peut-on vous aider ?</strong><textarea name="wordInput" rows="5"></textarea></label>

                <?php if (isset($_GET['courriel']) && $_GET['courriel']): ?>
                    <button class="btn btn-1 btn-1f courrielSent" disabled>Merci pour ce message ! </button>
                <?php else: ?>
                    <button class="btn btn-1 btn-1f" type="submit">Envoyez nous ce message ! </button>
                <?php endif ?>
            </form>

            </column>
            <column itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <p class="noSwipe">
                Sinon,<br/>
                    Vous pouvez nous <i class="icon fa flaticon-customer-service" aria-hidden="true"></i>
                    <br/><strong><a href="tel:+33638843319"><span itemprop="telephone">+33.(0)6.38.84.33.19</span></a></strong>
                </p>
                <p class="noSwipe">
                    ou bien nous envoyer un <i class="icon fa flaticon-new-email-interface-symbol-of-black-closed-envelope" aria-hidden="true"></i> si vous le préférez
                    <br/>
                    <strong><a href="mailto:contact@ltn.agency?subject=Au%20sujet%20de%20mon%20futur%20site"><span itemprop="email">contact@ltn.agency</span></a>
                    <br/>
                        <span itemprop="name">LTN</span>
                        <span><br/><span itemprop="streetAddress">32 rue des Vaures</span>,
                            <br/><span itemprop="postalCode">24100</span> <span itemprop="addressLocality">Bergerac</span>
                            <br/><span itemprop="addressCountry">France</span></span>
                    </strong>
                </p>
                <p>
                    Ou alors, vous pouvez toujours passer nous voir <i class="icon fa flaticon-meeting" aria-hidden="true"></i>
                    <br/>
                     <map><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2828.073106622815!2d0.4883670515844505!3d44.86080647899598!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aad088f913918b%3A0x34e933a2a698865c!2s32+Rue+des+Vaures%2C+24100+Bergerac!5e0!3m2!1sfr!2sfr!4v1447885478996" width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></map>
                <small><a target="_blank" href="https://www.google.fr/maps/place/32+Rue+des+Vaures,+24100+Bergerac/@44.8608065,0.4883671,17z/data=!3m1!4b1!4m2!3m1!1s0x12aad088f913918b:0x34e933a2a698865c">agrandir le plan</a></small>
                </p>
            </column>
        </contacts>
    </section-content>

<footer class="noSwipe" id="smallFooter">
    <span itemprop="legalName">LTN</span> - SASU au capital de 1000&euro;
    | N° TVA : FR 96 81 43 03 582
    | N° SIRET : 814 303 582
    | RIB : 20041 01001 2088761L022 88
</footer>
</section>
