<section id="section-accueil">
    <a class="changePage" href="#accueil"></a>
    <section-content>
        <logo itemprop="logo" content="<?php echo $fullUrl; ?>tarsier.php?color=090909&background=6B91AB">
            <?php echo file_get_contents('assets/logo-tarsier.svg'); ?>
            <?php echo file_get_contents('assets/logo-text.svg'); ?>
            <baseline>
                <hr/>
                <strong>Stratégie</strong> &amp; <strong>Développement Web</strong>
            </baseline>
            </logo>
        <presentation itemprop="description">
            <strong>Cr&eacute;ation de sites Internet, aide pour vos projets&nbsp;:</strong><br/>
            <strong>LTN</strong>, votre entreprise à <strong>Bergerac</strong> en <strong>Dordogne</strong> avec une présence <strong>internationale</strong> et qui accompagne <strong>vos ambitions numériques</strong>
        </presentation>
        <temoignages>
            <quote>«&nbsp;La collaboration avec LTN s'est révélée aussi cordiale qu'efficace. Ponctuel, disponible et réactif, Joël s'est montré à l'écoute de nos besoins qu'il a su parfaitement traduire en fonctionnalités du site. Le site créé avec lui répond aux besoins de notre institution tant pour les options proposées au public que pour la gestion interne de notre activité.&nbsp;»
                <author>Marianne de Préville - Coordinatrice des formateurs CEMEA</author>
            </quote>
            <quote>«&nbsp;J'ai eu l'honneur de côtoyer Joël pendant plusieurs mois au quotidien. Collègue toujours souriant, son côté jovial cache une efficacité impressionnante que son goût pour les claviers mécaniques ne vient aucunement altérer.&nbsp;»
                <author>Malo Pichot - Développeur Bob el Web</author>
            </quote>
            <quote>«&nbsp;Joël aime être challengé, il prend les choses à c&oelig;ur mais sait garder la distance nécessaire pour mener à bien ses projets. Il est un collaborateur très carré mais peut s’adapter à des méthodes de travail plus souples. Enfin, il porte un regard intelligent sur l’ensemble du projet et saura dire si quelque chose cloche dans le brief.&nbsp;»
                <author>Sandra Berkoukeche - Chef de projet Mairie de Paris</author>
            </quote>
        </temoignages>
    </section-content>
</section>
