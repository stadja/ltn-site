<section id="section-presentation">
    <a class="changePage" href="#presentation"></a>
    <section-content>
        <baseline>
            Qui y a-t-il derrière <strong>LTN</strong> ?
        </baseline>
        <presentation>
            <description itemprop="founder" itemscope itemtype="http://schema.org/Person">
              <meta itemprop="gender" content="Male">
              <meta itemprop="birthDate" content="June 17, 1985">
              <meta itemprop="email" content="stadja@gmail.com">
                <block class="withImage">
                    <img itemprop="image" src="https://ltn.agency/assets/images/costume.jpg" alt="">
                </block>
                <block>
                Bonjour ! Je m'appelle <strong itemprop="name" content="Jo&euml;l Isra&euml;l">Jo&euml;l Isra&euml;l</strong>, je suis <strong>freelance</strong> et je fais du <strong>développement Web</strong>.
                <br/><br/>
                J'ai une trentaine d'années, je suis Franco-Suisse et cela fait bientôt 10 ans que je travaille dans le domaine. Après mes études en Suisse à l'EPFL, j'ai eu la chance de travailler dans des milieux divers et variés&nbsp;: de MyTF1 à la mairie de Paris, en passant par une startup bordelaise et du consulting en Suisse...
                <br/>Je vis aujourd'hui à Bergerac en Dordogne. En voyant l'état du développement numérique, notamment dans les PME, j'ai eu envie de créer <strong>LTN</strong> pour aider ces entreprises qui souvent savent qu'elles ont un besoin, mais ne savent pas comment, ni quoi faire pour se développer sur le Web.
                <br/><br>
                Je travaille donc à mon compte depuis fin 2014 et <strong>L</strong>e&nbsp;<strong>T</strong>arsier&nbsp;<strong>N</strong>umérique est né en décembre 2015.
                <br/>
                <br/>
                Mais rassurez vous ! Je ne suis pas qu'informaticien&nbsp;: j'aime aussi jouer de la guitare, manger des pâtes et renifler les fleurs.
                <br/>
                <a href="http://stadja.net/now">stadja.net</a><br/>
                <a href="assets/joel-israel-cv.pdf">Mon CV</a>
                </block>
                <block>
                    <tarsier><img src="tarsier.php?color=6B91AB&background=BCD5E2"/></tarsier>
                </block>
            </description>
        </presentation>
    </section-content>
</section>
