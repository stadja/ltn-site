<section id="section-services">
    <a class="changePage" href="#services"></a>
    <section-content itemprop="hasOfferCatalog" itemscope itemtype="http://schema.org/OfferCatalog">
        <baseline itemprop="potentialAction">
            Pour tous vos besoins numériques... <strong>LTN</strong> est là !
        </baseline>
        <services>
            <service itemprop="itemListElement" itemscope itemtype="http://schema.org/OfferCatalog">
                <h1 itemprop="name">Études de besoins</h1>
                <p itemprop="description">
                    Chaque professionnel travaille différemment, chaque mission a ses spécificités, chaque projet a ses propres objectifs. Ensemble, nous établirons le <strong>cahier des charges</strong> de vos besoins afin que vous puissiez facilement <strong>développer l'outil de vos rêves</strong>.
                </p>
            </service>
            <service itemprop="itemListElement" itemscope itemtype="http://schema.org/OfferCatalog">
                <h1 itemprop="name">Créations de sites Internet et développements Web</h1>
                <p itemprop="description">
                    C'est notre <i class="icon fa flaticon-favorite-heart-button" aria-hidden="true"></i> de métier, notre spécialité, notre dada !<br/>
                    Du site Internet vitrine pour votre entreprise, ou du blog classique (mais bien fini !) à la web app ultra-dynamique et interactive, en passant par la gestion de stocks, la génération de factures ou le débogage de système existant... <strong>tout est imaginable, alors travaillons ensemble !</strong>
                </p>
            </service>
            <service itemprop="itemListElement" itemscope itemtype="http://schema.org/OfferCatalog">
                <h1 itemprop="name">Conseils et prestations informatiques</h1>
                <p itemprop="description">Besoin d'un hébergement pour votre site web, d'un nouvel ordinateur, d'un système de synchronisation interne à votre entreprise, de la configuration de votre anti virus... <strong>LTN est là pour vous !</strong></p>
            </service>
            <service>
                <tarsier><img src="tarsier.php?color=6B91AB&background=BCD5E2"/></tarsier>
            </service>
        </services>
    </section-content>
</section>
