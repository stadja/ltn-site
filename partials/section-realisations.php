<section id="section-realisations">
    <a class="changePage" href="#realisations"></a>
    <section-content>
        <baseline>
            Quelques réalisations de <strong>LTN</strong>
        </baseline>
        <realisations>

            <ul id="og-grid" class="og-grid">
                <li class="noSwipe" id="braquessac">
                    <a href="<?php echo $url; ?>realisations/braquessac" data-href="" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/bureaubraquessac-screen.png" data-title="Bureau Braquessac" data-description="Le site gère toutes les données professionnelles d'un courtier en vin: les stocks, les clients, les vendeurs, les chateaux... De plus le site doit gérer tous les aspects des transactions, de la création d'offre à la facturation et aux suivis de paiements et de la commande à l'enlèvement. Enfin le site doit aussi être hébergé avec une sauvegarde du contenu et de la base de donnée très régulière et sécurisée." data-techno="Laravel 5, Bootstrap" data-contraintes="Puisque le site couvre tous les besoins de l'entreprise, une étude approfondie du fonctionnement doit être menée et une discussion constante avec le client doit être menée pendant tout le développement.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/bureaubraquessac.png" alt="Le site des Bureau Braquessac"/>
                    </a>
                </li>
                <li class="noSwipe" id="cemea">
                    <a href="<?php echo $url; ?>realisations/cemea" data-href="http://formation-cemea.ch" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/cemea-screen.png" data-title="Le site des CEMEA" data-description="Refonte complète du site des Centres d’entraînement aux méthodes d’éducation active (CEMEA). Le site gère tous les aspects de la vie de l'association (de la création de cours à l'inscription des étudiants et formateurs en passant par la gestion des factures)." data-techno="Wordpress 4" data-contraintes="Les administrateurs doivent avoir complètement la main pour modifier tous les aspects du site.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/cemea-square.png" alt="Le site des CEMEA"/>
                    </a>
                </li>
                <li class="noSwipe" id="quefaire">
                    <a href="<?php echo $url; ?>realisations/quefaire" data-href="http://quefaire.paris.fr" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/quefaire-screen.jpeg" data-title="Que faire &agrave; Paris" data-description="Architecture et conception du site de la ville de Paris pour dénicher bons plans culturels, sorties et activités." data-techno="CodeIgniter 2, jQuery, Google Analytics, Git" data-contraintes="Beaucoup d'acteurs en jeu, plus d'une centaine de contributeurs, un très fort traffic.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/quefaire-logo.png" alt="Que Faire &agrave; Paris ?"/>
                    </a>
                </li>
                <li class="noSwipe" id="vdlv">
                    <a href="<?php echo $url; ?>realisations/vdlv" data-href="" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/vdlv-screen.png" data-title="Vincent Dans Les Vappes" data-description="Nous travaillons avec l'entreprise VDLV pour améliorer la partie de leur site destinée aux professionnels: que ce soit les commandes en gros, la gestion des stocks, des clients, des avoirs, des factures..." data-contraintes="Le site a été mis en place par une entreprise qui a ensuite abandonné son développement. Nous devons donc intervenir sur un site dont nous n'avons pas toutes les clés de compréhension ni la logique du premier développeur: beaucoup d'ingénierie inverse est nécessaire.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/vdlv-logo.png" alt="Le site de VDLV"/>
                    </a>
                </li>
                <li class="noSwipe" id="ynternet">
                    <a href="<?php echo $url; ?>realisations/ynternet" class="viewer" data-href="http://www.ynternet.org/" data-hreflabel="Voir le site de la fondation" target="_blank" data-title="La fondation Ynternet.org" data-description="Créée en 1998, Ynternet.org est un institut de recherche et formation dont l’objectif est de promouvoir une utilisation des technologies de l’information qui concilie efficacité économique et éthique numérique, tout en s’appuyant sur les bonnes pratiques propres à la citoyenneté numérique.
                    <br/><strong>LTN</strong> est mandaté par la fondation dans de nombreux projets de recherche, notamment dans les projets associés avec la Haute École de Gestion de Genève. Notre société est aussi responsable de la gestion des divers sites de la fondation." data-techno="Blockchains, IPFS, Stellar..." data-contraintes="La plupart des technologies utilisées dans ces projets sont des technologies de pointes et très novatrices. LTN est fière d'aider la recherche informatique grâce aux projets de la fondation.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/ynternet.png" alt="Le site des CEMEA"/>
                    </a>
                </li>
                <li class="noSwipe" id="pragmaticdreamers">
                    <a href="<?php echo $url; ?>realisations/pragmaticdreamers" data-href="http://www.pragmaticdreamers.fr" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/pragmatic-dreamers-screen.png" data-title="Pragmatic Dreamers" data-description="Développement du site promotionnel d'une agence de coaching." data-techno="Slide framework" data-contraintes="Site graphique développé avec un budget limité.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/pragmatic-dreamers-logo.png" alt="Pragmatic Dreamers"/>
                    </a>
                </li>
                <li class="noSwipe" id="placealemploi">
                    <a href="<?php echo $url; ?>realisations/placealemploi" data-href="http://www.placealemploi.fr" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/placealemploi-screen.jpg" data-title="Place à l'emploi 2015" data-description="Développement du site de l'édition 2015 de l'événement Place à l'Emploi." data-techno="Symfony 2, Bootstrap 3, jQuery, Git" data-contraintes="Peu de temps de développement pour un site complet et devant supporter un gros traffic.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/pal_square.png" alt="Place à l'emlpoi 2015"/>
                    </a>
                </li>
                <li class="noSwipe" id="mequipement">
                    <a href="<?php echo $url; ?>realisations/mequipement" data-href="http://m.equipement.paris.fr/" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/webapp-equipement-screen.png" data-title="La Webapp des équipements de la Ville de Paris" data-description="Conception d'un site pour accès mobile regroupant les informations sur tous les équipements de la ville de Paris avec gestion des favoris." data-techno="CodeIgniter, jQuery, Bootstrap" data-contraintes="Le site est la vue par défault dès que l'on visite les sites des équipements de la ville avec une plateforme mobile: il fallait un site s'adaptant à tous formats nomades.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/webapp-equipement-logo.png" alt="Le Mero Pièces Auto"/>
                    </a>
                </li>
                <li class="noSwipe" id="reserve">
                    <a href="<?php echo $url; ?>realisations/reserve" data-href="http://satellite.mediapart.fr/reserve-parlementaire/" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/duflot-screen.png" data-title="Mediapart - Formulaires adaptatifs" data-description="Conceptions de différents formulaires intégrés au sein d'articles d'actualité sur le site mediapart.fr." data-techno="Bootstrap, jQuery, Slim Framework, Git, PHP">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/mediapart-logo.png" alt="Mediapart - Loi Duflot: calculez votre futur loyer"/>
                    </a>
                </li>
                <li class="noSwipe" id="lemero">
                    <a href="<?php echo $url; ?>realisations/lemero" data-href="http://www.lemeropiecesauto.fr" class="viewer" target="_blank" data-largesrc="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/lemeropiecesauto-screen.jpeg" data-title="Le Mero Pièces Auto" data-description="Conception d'un site de vente de pièces auto pour un garage local." data-techno="CodeIgniter 3, Grocery Crud, jQuery, Bootstrap 3" data-contraintes="Un site fonctionnel pour un budget tr&egrave;s limit&eacute;.">
                        <img src="https://res.cloudinary.com/stadja/image/fetch/f_auto,q_auto/<?php echo $domain; ?>assets/images/lemeropiecesauto-logo.png" alt="Le Mero Pièces Auto"/>
                    </a>
                </li>
            </ul>

        </realisations>
    </section-content>
</section>
