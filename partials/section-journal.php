<section id="section-journal">
    <a class="changePage" href="#journal"></a>
    <section-content>
        <baseline>
            Le journal du Tarsier
        </baseline>
        <br/>
        <p>Une collection d'articles intéressants pour vous et pour nous !</p>
        <journal>
            <div><i id="emptySearch" <?php if ($article): ?> style="opacity: 1;" <?php endif;?> class="icon fa flaticon-cancel" aria-hidden="true"></i></div>
            <ul class="journal-list">
                <?php if (!$article): ?><?php include '_journal-posts.php';?><?php endif;?>
            </ul>
            <ul class="journal-list-search">
                <?php if ($article): ?><?php include '_journal-posts.php';?><?php endif;?>
            </ul>
        </journal>

    </section-content>
</section>
