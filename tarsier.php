<?php
$color = 'black';
if (isset($_GET['color']) && $_GET['color']) {
    $color = $_GET['color'];
    if (ctype_xdigit($color)) {
        $color = '#'.$color;
    }
}

$background = 'none';
if (isset($_GET['background']) && $_GET['background']) {
    $background = $_GET['background'];
    if (ctype_xdigit($background)) {
        $background = '#'.$background;
    }
}
try {
    $im = new Imagick();
    $svg = file_get_contents('assets/logo-tarsier-generator.svg');
    $svg = str_replace('FILLCOLOR', $color, $svg);

    $im->setBackgroundColor($background);
    $im->readImageBlob($svg);
    $im->setImageFormat("png");

    $im->writeImage(getcwd().'/assets/upload/image.png');
} catch(Exception $e) {
    var_dump($e);
    var_dump($_FILES);
    die();
}

header('Content-type: image/png');
echo $im;

$im->clear();
$im->destroy();
?>
